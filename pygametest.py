import pygame
import os
import time
import math
# version 0.4

class Wall:
    x = 0
    y = 0
    w = 10
    h = 30
    startage = 0
    age = 0
    maxage = 600
    rect = None

    def __init__(self, pl):

        self.y = pl.y + (pl.h / 2)
        if pl.spellDirection == 1:
            self.x = pl.x + pl.w
            self.pos = True
        else:
            self.pos = False
            self.x = pl.x - self.w
        self.startage = math.floor(100 * time.time())
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)

    def draw(self):
        return self.rect

    def update(self):
        self.age = math.floor(100 * time.time())
        if self.age - self.startage >= self.maxage:
            return True
        else:
            return False


class Player:
    x = 0
    y = 0
    w = 0
    h = 0

    rect = None

    inc = 5
    slowinc = inc
    slowed = False

    spellDirection = 0

    fireballStats = dict(fireballCooldown=300000, lastFireball=0)

    wallStats = dict(wallCooldown=5000000, lastWall=0)

    def __init__(self, x, y, w, h, color):
        self.color = color
        self.rect = pygame.Rect(x, y, w, h)
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def update(self):
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)
        return self.rect

    def draw(self, screen):
        pygame.draw.rect(screen, color, p.update())


class Fireball:
    x = 0
    y = 0
    inc = 10
    w = 10
    h = 10
    rect = None

    def __init__(self, pl):
        self.y = pl.y + (pl.h / 2)
        if pl.spellDirection == 1:
            self.x = pl.x + pl.w + 15
            self.pos = True
        else:
            self.pos = False
            self.x = pl.x - 15
        if not self.pos:
            self.inc *= -1
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)

    # When called, move the fireball in the direction it was cast
    def update(self):
        self.x += self.inc
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)
        return self.rect

    def getRect(self):
        return self.rect


def nanotime():
    return math.floor(1000000 * time.time())


# I have no idea what SDL... does
os.environ['SDL_VIDEO_CENTERED'] = '1'

# init pygame
pygame.init()

try:
    # fireball list
    fireballs = []
    fireballBuffer = Fireball.w + 2

    # Wall list
    walls = []
    wallBuffer = Wall.w * 4

    # screen dimensions
    screenWidth = 500
    screenHeight = 600

    # create the screen
    screen = pygame.display.set_mode((screenWidth, screenHeight))

    # are we done with the game
    done = False

    # make a player

    color = (0, 128, 255)
    p = Player(30, 30, 30, 60, color)
    # make a game clock
    clock = pygame.time.Clock()

    # main Game loop
    while not done:
        nano = nanotime()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        pressed = pygame.key.get_pressed()

        # Begin Movement
        if (pressed[pygame.K_UP] or pressed[pygame.K_DOWN]) and (pressed[pygame.K_LEFT] or pressed[pygame.K_RIGHT]):
            if not p.slowed:
                p.slowinc = p.inc
                p.inc = .75 * p.inc
                p.slowed = True
        else:
            if p.slowed:
                p.inc = p.slowinc
                p.slowed = False

        if pressed[pygame.K_UP]:
            if p.y - p.inc >= 0:
                p.y -= p.inc
            else:
                resetinc = p.inc
                p.inc = p.y
                p.y -= p.inc
                p.inc = resetinc

        if pressed[pygame.K_DOWN]:
            if p.y + p.h + p.inc <= screenHeight:
                p.y += p.inc
            else:
                resetinc = p.inc
                p.inc = screenHeight - p.h - p.y
                p.y += p.inc
                p.inc = resetinc

        if pressed[pygame.K_LEFT]:
            if p.x - p.inc >= 0:
                p.x -= p.inc
            else:
                resetinc = p.inc
                p.inc = p.x
                p.x -= p.inc
                p.inc = resetinc

        if pressed[pygame.K_RIGHT]:
            if p.x + p.w + p.inc <= screenWidth:
                p.x += p.inc
            else:
                resetinc = p.inc
                p.inc = screenWidth - p.w - p.x
                p.x += p.inc
                p.inc = resetinc
        # End Movement

        # Begin fireball spell
        if pressed[pygame.K_s]:
            if pressed[pygame.K_d] or pressed[pygame.K_a]:
                if pressed[pygame.K_SPACE]:
                    fire = False
                    if p.fireballStats["lastFireball"] == 0:
                        # First fireball cast Ever
                        p.fireballStats["lastFireball"] = nano
                        fire = True
                    elif nano - p.fireballStats["lastFireball"] > p.fireballStats["fireballCooldown"]:
                        fire = True

                    if fire:
                        if pressed[pygame.K_d]:
                            p.spellDirection = 1
                        else:
                            p.spellDirection = -1
                        f = Fireball(p)
                        fireballs.append(f)
                        p.fireballStats["lastFireball"] = nano

        # Begin Wall spell
        if pressed[pygame.K_w]:
            if pressed[pygame.K_d] or pressed[pygame.K_a]:
                if pressed[pygame.K_SPACE]:
                    fire = False
                    if p.wallStats["lastWall"] == 0:
                        # First fireball cast Ever
                        p.wallStats["lastWall"] = nano
                        fire = True
                    elif nano - p.wallStats["lastWall"] > p.wallStats["wallCooldown"]:
                        fire = True

                    if fire:
                        if pressed[pygame.K_d]:
                            p.spellDirection = 1
                        else:
                            p.spellDirection = -1
                        w = Wall(p)
                        walls.append(w)
                        p.wallStats["lastWall"] = nano

        # If q is pressed, print basic stats about the player
        if pressed[pygame.K_q]:
            stats = ""
            stats += "x" + str(p.x)
            stats += ", "
            stats += "y" + str(p.y)
            stats += ", "
            stats += "w" + str(p.w)
            stats += ", "
            stats += "h" + str(p.h)
            stats += ", "
            stats += "inc" + str(p.inc)
            print(stats)
        # Paint the screen Black
        screen.fill((0, 0, 0))
        # Create the players rectangle

        # for each fireball
        for f in fireballs:
            # should i draw this?
            skip = False

            # if the fireballs x coord are beyond the screen width, remove it and skip
            if f.x < -fireballBuffer or f.x > screenWidth + fireballBuffer:
                fireballs.remove(f)
                skip = True
            # if the fireballs y coord are beyond the screen height, remove it and skip
            if f.y < -fireballBuffer or f.y > screenHeight + fireballBuffer:
                fireballs.remove(f)
                skip = True
            # if it still exists, draw it

            for w in walls:
                if f.getRect().colliderect(w):
                    f.inc *= -1
            if not skip:
                pygame.draw.rect(screen, (255, 0, 0), f.update())

        # for each Wall
        for w in walls:
            # should i draw this?
            skip = False

            # if the Wall is old
            if w.update():
                walls.remove(w)
                skip = True
            # if it still exists, draw it
            if not skip:
                pygame.draw.rect(screen, (0, 255, 0), w.draw())

        # Draw the players Rectangle
        p.draw(screen)

        # refresh the screen
        pygame.display.flip()

        # set the FPS
        clock.tick(60)
finally:
    pygame.quit()  # Keep this IDLE friendly
